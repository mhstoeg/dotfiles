" All system-wide defaults are set in $VIMRUNTIME/archlinux.vim (usually just
" /usr/share/vim/vimfiles/archlinux.vim) and sourced by the call to :runtime
" you Zan find below.  If you wish to change any of those settings, you should
" do it in this file (/etc/vimrc), since archlinux.vim will be overwritten
" everytime an upgrade of the vim packages is performed.  It is recommended to
" make changes after sourcing archlinux.vim since it alters the value of the
" 'compatible' option.

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages.
runtime! archlinux.vim

" If you prefer the old-style vim functionalty, add 'runtime! vimrc_example.vim'
" Or better yet, read /usr/share/vim/vim80/vimrc_example.vim or the vim manual
" and configure vim to your own liking!

" do not load defaults if ~/.vimrc is missing
"let skip_defaults_vim=1

" CSI Ps SP q
"    Set cursor style (DECSCUSR, VT520).
"    Ps = 0  -> blinking block.
"    Ps = 1  -> blinking block (default).
"    Ps = 2  -> steady block.
"    Ps = 3  -> blinking underline.
"    Ps = 4  -> steady underline.
"    Ps = 5  -> blinking bar (xterm).
"    Ps = 6  -> steady bar (xterm).
" Set insert mode
let &t_SI = "\e[5 q"
" Set normal mode
let &t_EI = "\e[1 q"
" Set replace mode to a underline
let &t_SR = "\e[4 q"

"inoremap kj <ESC>
"inoremap jk <ESC>
set timeoutlen=100 ttimeoutlen=0
set number
syntax on
set noswapfile
set hlsearch
set incsearch
highlight MatchParen gui=bold guibg=NONE guifg=lightblue cterm=bold ctermbg=NONE
":nnoremap j h
":nnoremap k j
":nnoremap h i
":nnoremap i k

":vnoremap j h
":vnoremap k j
":vnoremap l k
":vnoremap ; l
":vnoremap h ;
"set spellang=en_us
