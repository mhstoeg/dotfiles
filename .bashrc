#
# ~/.bashrc
#

### ALIASES ###
alias vim='nvim'
alias g='git '
alias ga='g add '
alias gc='g commit -m '
alias gs='g status'
alias gpush='g push '
alias gpull='g pull '
alias gco='g checkout '
alias grc='g rebase --continue '
alias gra='g rebase --abort '
alias grs='g rebase --skip '
alias dg='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
alias dga='dg add '
alias dgc='dg commit -m '
alias dgs='dg status'
alias dgpush='dg push git@gitlab.com:mhstoeg/Dotfiles.git'
alias dgpull='dg pull '
alias dgco='dg checkout '
alias dgrc='dg rebase --continue '
alias dgra='dg rebase --abort '
alias dgrs='dg rebase --skip '

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export PATH="$PATH:$HOME/bin"

# Set up Vim-style tab completion
bind 'TAB: menu-complete'
set show-all-if-ambiguous on

# If a tab-completed file is a symlink to a directory,
# treat it like a directory not a file
set mark-symlinked-directories on

# enable colors
eval "`dircolors -b`"

	
# Use GNU ls colors when tab-completing files
set colored-stats on


# make grep highlight results using color
export GREP_OPTIONS='--color=auto'

# Add some colour to LESS/MAN pages
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;33m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;42;30m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;36m'

# bash_completion
if [ -f /etc/bash_completion ]; then
  source /etc/bash_completion
fi
