# Dotfiles

Dotfiles for all my linux systems

# System Recovery using Live-USB ISO:

Boot the Arch Linux ISO on the desire machine

Connect to the Internet using ```iwctl```

Use fdisk to determine efi_system_partition and boot_partition

```
fdisk -l
```

Mount the efi system partition

```
mkdir /mnt/boot
```

```
mount /dev/<efi_system_partition> /mnt/boot
```

Mount the boot partition

```
mount /dev/<boot_partition> /mnt
```

Change root into the system

```
arch-chroot /mnt
```

Make any neccessary changes

