# Arch Linux Installation

> NOTE: Some configurations and steps may not be applicable you.
        This guide was created by mhstoeg to aid 
        in their personal installs of arch linux.

## 1.0 - Network Configuration

Run the following commands to setup a wifi connection.
If there is no need to connect to wifi, proceed to 2.0.

### 1.1 - Enter iwctl

```
iwctl
```

### 1.2 - Check for wifi module

```
device list
```

> DO NOT proceed with the installation if no module is found.
> It is recommend to understand the cause of error if you know
> for centain that a module should be found.

### 1.3 - Scan for available networks

```
station <"wlan"> scan
```

### 1.4 - Check for avaliable networks

Replace <"wlan"> with a wifi module found in section 1.2.

```
station <"wlan"> get-networks
```

### 1.5 - Connect to a network

Replace <"network name"> with a network name found in section 1.3.

```
station <"wlan"> connect <"network name">
```

Enter the passphrase when prompted.

### 1.6 - Verify connection

```
station list
```

If connected, its state will be 'connected'.

### 1.5 - Exit

```
exit
```

### 1.6 - Ping a website for further verification

```
ping www.archlinux.org
```

Packets of data should be received.

## 2.0 Drive Configuration

> NOTE: This does not include non-UEFI and UEFI encrytion configurations

Run the following commands to setup and partition drives. If
the drive is already partitioned, proceed to section 2.8.

### 2.1 Determine Drives

```
fdisk -l
```

### 2.2 Enter fdisk

Replace <"drive name"> with a drive name found in section 2.1.

```
fdisk <"drive name">
```

### 2.3 Create Partition Table

> NOTE: Create partition table if and only if you wish to wipe the entire disk

```
g
```

### 2.4 Setup EFI Partition

#### 2.4.1 Create EFI Partition

```
n
```
> NOTE: The EFI Partition should be at least 1GiB

For partition number, enter a number or hit ENTER for default.

For first sector, enter a number or hit ENTER for default.

For last sector, enter ```+1G``` then hit ENTER.

#### 2.4.2 Set type for EFI Partition

```
t
```

Make sure the correct partition is selected.

For partition type, enter ```1``` then hit ENTER

### 2.5 Setup Swap Partition

#### 2.5.1 Create Swap Partition

```
n
```

For partition number, enter a number or hit ENTER for default.

For first sector, enter a number or hit ENTER for default.

For last sector, enter ```+8000M``` (8GB) or ```+16000M``` (16GB)
then hit ENTER.

#### 2.5.2 Set type for Swap Partition

```
t
```

Make sure the correct partition is selected.

For partition type, enter ```19``` then hit ENTER

### 2.6 Setup Linux Filesystem Partition

#### 2.6.1 Create Linux Filesystem Partition

```
n
```

For partition number, enter a number or hit ENTER for default.

For first sector, enter a number or hit ENTER for default.

For last sector, specify a size like ```+50000M``` (50GB) then hit ENTER,
or hit enter to take up the remaining volume of the disk.

This drive should already be set to the correct type of 'Linux filesystem'.

### 2.7 Write Changes

> NOTE: Make sure you are happy with the changes before writing. Enter the print command 
'p' to display the disk partitions.
```
w
```

Use ```fdisk -l``` to verify the partitions

### 2.8 Format Partitions

#### 2.8.1 Format EFI Partition

```
mkfs.fat -F32 /dev/<efi_system_partition>
```

#### 2.8.2 Format Swap Partition

```
mkswap /dev/<swap_partition>
```

#### 2.8.3 Format Root Partition

```
mkfs.ext4 /dev/<root_partition>
```

### 2.9.0 Encrypt Root Partition

>NOTE: Skip this step if you do not wish to encrypt your root partition.

#### 2.9.1 Encrypt root partition with cryptsetup

```
cryptsetup luksFormat /dev/<root_partition>
```
When prompted, enter 'YES', then hit ENTER.

Enter your passphrase, then a second time to verify it

#### 2.9.2 Unlock root partition to continue -IS THIS SECTION NEEDED?

```
cryptsetup open --type luks /dev/<root_partition> <encrypted_drive>
```

Enter your passphrase.

#### 2.9.3 Create physical volume for encrypted disk

```
pvcreate /dev/mapper/<encrypted_drive>
```

#### 2.9.4 Create volume group

```
vgcreate volgroup0 /dev/mapper/<encrypted_drive>
```

#### 2.9.5 Create logical volumes

```
lvcreate -L 30GB volgroup0 -n lv_root
```
```
lvcreate -L <home_partition_size>GB volgroup0 -n lv_home
```

#### 2.9.6 Verify volume group and logical volumes

```
vgdisplay
```
```
lvdisplay
```

#### 2.9.7 Insert kernal module (dm_mod)

```
modprobe dm_mod
```

#### 2.9.8 Scan for avaliable volume group

```
vgscan
```

#### 2.9.9 Activate volume groups

```
vgchange -ay
```

#### 2.9.10 Format root and home logical volumes

```
mkfs.ext4 /dev/volgroup0/lv_root
```
```
mkfs.ext4 /dev/volgroup0/lv_home
```



### 2.10 Mount the filesystems

#### 2.10.1 Mount the root volume to /mnt (and mount the home volume).

If you did NOT create an LVM:
```
mount /dev/<root_partiton> /mnt
```

If you did create an LVM, mount the root volume
```
mount /dev/volgroup0/lv_root /mnt
```
If you did create an LVM, mount the root volume
```
mkdir /mnt/home
```
```
mount /dev/volgroup0/lv_root /mnt/home
```

#### 2.10.2 Mount the EFI system partiton.

```
mkdir /mnt/boot
```

```
mount /dev/<efi_sysytem_partition> /mnt/boot
```

#### 2.10.3 Enable the swap volume.

```
swapon /dev/<swap_partition>
```

## 3.0 Installation

Installing components for Arch Linux to function.

### 3.1 Run pacstrap

```
pacstrap -i /mnt base
```

### 3.2 Generate filesystem table

#### 3.2.1 Generate fstab file

```
genfstab -U -p /mnt >> /mnt/etc/fstab
```

#### 3.2.2 Check the resulting file

```
cat /mnt/etc/fstab
```

### 3.3 Chroot

Change root into the new system.

```
arch-chroot /mnt
```

## 4.0 Configure System

Configure the Arch Linux Installation:

### 4.1 Install Some Packages

Add any packages required

```
pacman -S sudo gvim base-devel networkmanager wpa_supplicant wireless_tools dialog lvm2
```
```
pacman -S linux linux-headers linux-lts linux-lts-headers linux-firmware
```

### 4.2 Enable NetworkManager

```
systemctl enable NetworkManager
```

### 4.4 Localisation
 
#### 4.4.1 Choose Localisation

```
vim /etc/locale.gen
```

Uncomment the line that applies to your localisation:
e.g.
```en_AU.UTF-8 UTF-8```

Save the changes

#### 4.4.2 Generate the Locale File

```
locale-gen
```

### 4.5 Hostname

```
hostnamectl set-hostname <"hostname">
```

### 4.6 Set Root Password

```
passwd
```

### 4.7 Create User

#### 4.7.1 Add User

```
useradd -m -g users -G wheel <user_name>
```

#### 4.7.2 Set User's Password

```
passwd mark
```

### 4.8 Configure sudo

```
EDITOR=vim visudo
```

Uncomment the following line:

```
%wheel ALL=(ALL:ALL) ALL
```


## 5.0 Grub Installation

Install Grub

### 5.1 Install packages

```
pacman -S grub efibootmgr dosfstools os-prober mtools
```

### 5.1.2 Edit Grub

```
vim /etc/default/grub
```

Add ```cryptdevice=/dev/<"root_partition">:volgroup0``` to GRUB_CMDLINE_LINUX_DEFAULT=

Uncomment the following line to discover other operating systems:
```
GRUB_DISABLE_OS_PROBER=false
```


### 5.2 Make EFI Directory

```
mkdir /boot/EFI
```

### 5.3 Mount EFI Partition

```
mount /dev/<efi_system_partition> /boot/EFI
```

### 5.4 Install Grub

```
grub-install --target=x86_64-efi --bootloader-id=grub_uefi --efi-directory=/boot/EFI --recheck
```

Continue if no errors reported

```
cp /usr/share/locale/en\@quot/LC_MESSAGES/grub.mo /boot/grub/locale/en.mo
```

To discover windows operation system:
```
mkdir /mnt/win
```
```
os-prober
```
```
mount /dev/<windows_efi_partition> /mnt/win
```

### 5.5 Generate Grub Configuration File

```
grub-mkconfig -o /boot/grub/grub.cfg
```

### 5.6 Edit mkinitcpio.conf if setup encryption and LVM

```
vim /etc/mkinitcpio.conf
```

Add the following between block and filesystem to HOOKS line:
```
HOOKS=(...block encrypt lvm2 filesystems...): 
```

#### 5.6.1 mkinitcpio

```
mkinitcpio -p linux
```

If you also have the lts kernal:
```
mkinitcpio -p linux-lts
```

### 5.7 Exit and Unmount

```
exit
```

```
umount -a
```

### 5.8 Reboot

```
reboot
```

## 6.0 Post-installation

### 6.1 Connect to the internet if using wifi:

#### 6.1.1 Find available networks

```
nmcli dev wifi list
```

#### 6.1.2 Connect to the internet

```
nmcli dev wifi connect <SSID> password <password>
```

### 6.2 Become root user to execute the next commands:

```
su
```

### 6.3 To remove the need to enter the password for a specific user

```
echo '<user> ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
```

### 6.4 Install System's Microcode

#### 6.4.1 AMD

```
pacman -S amd-ucode
```

#### 6.4.2 INTEL

```
pacman -S intel-ucode
```

### 6.5 Install Video Driver:

#### 6.5.1 INTEL or AMD GPU

```
pacman -S mesa
```

To enable hardware decoding on intel GPU (Broadwell or newer)
```
pacman -S intel-media-driver
```

To enable hardware decoding on AMD GPU
```
pacman -S libva-mesa-driver
```

#### 6.5.2 Nvidia GPU

```
pacman -S nvidia nvidia-utils nvidia-lts
```

### 6.6 Install Xorg

```
pacman -S xorg-server xorg-xinit
```

### 6.7 Install Git

```
pacman -S git
```

### 6.8 Install and Setup yay

#### 6.8.1 Clone repository 

```
sudo git clone https://aur.archlinux.org/yay.git
```

#### 6.8.2 Give permissions to user

```
sudo chown -R <username>:users yay
```

#### 6.8.3 Make the package

```
cd yay
```

```
makepkg -si
```

### 6.9 Timezone

#### 6.9.1 Set Timezone

```
timedatectl set-timezone Australia/Melbourne
```

#### 6.9.2 Sync Timezone

```
systemctl enable systemd-timesyncd
```


Congratulations, you have successfully installed Arch Linux (and done a bit more) !
To configure specific systems, select the correct branch on
https://gitlab.com/mhstoeg/dotfiles and go through USER_CONFIG.md
